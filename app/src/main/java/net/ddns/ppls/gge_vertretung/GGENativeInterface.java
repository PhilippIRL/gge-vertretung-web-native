package net.ddns.ppls.gge_vertretung;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

public class GGENativeInterface {
    protected MainActivity context;

    GGENativeInterface(MainActivity c) {
        context = c;
    }

    @JavascriptInterface
    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    @JavascriptInterface
    public void setBarColor(final String color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    context.getWindow().setStatusBarColor(Color.parseColor(color));
                }
            });
        }
    }

    @JavascriptInterface
    public void exitApplication() {
        context.finish();
    }

    @JavascriptInterface
    public boolean canShare() {
        return true;
    }

    @JavascriptInterface
    public void share(String data, String text) {
        try {

            String base64str = data.replace("data:image/png;base64,", "");
            byte[] decoded = Base64.decode(base64str, Base64.DEFAULT);

            File outputDir = context.getCacheDir();
            File file = new File(outputDir.getAbsolutePath() + "/share.png");
            if(file.exists()) {
                file.delete();
            }

            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(decoded);
            bos.close();
            file.setReadable(true, false);

            Uri uri = FileProvider.getUriForFile(context, "net.ddns.ppls.gge_vertretung.fileprovider", file);

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            shareIntent.setType("image/png");

            Intent chooserIntent = Intent.createChooser(shareIntent, "Share");

            /* https://stackoverflow.com/a/39214019 */

            chooserIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            List<ResolveInfo> resInfoList=
                    context.getPackageManager()
                            .queryIntentActivities(chooserIntent, PackageManager.MATCH_DEFAULT_ONLY);

            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

            context.startActivity(chooserIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Unable to share", Toast.LENGTH_SHORT).show();
        }
    }

    @JavascriptInterface
    public String getAppName() {
        return context.getString(R.string.app_name);
    }
}
